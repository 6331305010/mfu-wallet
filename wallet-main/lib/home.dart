import 'package:flutter/material.dart';
import 'page/chat.dart';
import 'page/dashboard.dart';
import 'page/setting.dart';
import 'page/profile.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;
  final List<Widget> screens = [
    Dashboard(),
    Profile(),
    Chat(),
    Setting(),
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Dashboard();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.account_balance),
        onPressed: () {},
        backgroundColor: Colors.yellow.shade900,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.yellow.shade900,
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Dashboard();
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 0 ? Colors.white : Colors.white,
                        ),
                        Text('Home',
                            style: TextStyle(
                              color:
                                  currentTab == 0 ? Colors.white : Colors.white,
                            ))
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Chat();
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.paid,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text('Transfer',
                            style: TextStyle(
                              color:
                                  currentTab == 1 ? Colors.white : Colors.white,
                            ))
                      ],
                    ),
                  )
                ],
              ),
///////////////////////////////////////Right Tab Bar Icons/////////////////////////////////////////////////////////////////////
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Profile();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.qr_code,
                          color: currentTab == 2 ? Colors.white : Colors.white,
                        ),
                        Text('Scan/MyQR',
                            style: TextStyle(
                              color:
                                  currentTab == 2 ? Colors.white : Colors.white,
                            ))
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Setting();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.more_horiz,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text('More',
                            style: TextStyle(
                              color:
                                  currentTab == 1 ? Colors.white : Colors.white,
                            ))
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
