import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
        leading: IconButton(onPressed: () {}, icon: Icon(Icons.account_circle)),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.notifications_none)),
          IconButton(onPressed: () {}, icon: Icon(Icons.power_settings_new)),
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.yellow.shade900, Colors.red.shade900],
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
            ),
          ),
        ),
        elevation: 20,
        titleSpacing: 20,
      ),

      // body : Container(child: GridView(children: gridDelegate[

      // ]

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Image.network(
                  'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/270243716_296428629147622_1215277136535552818_n.png?_nc_cat=107&ccb=1-5&_nc_sid=ae9488&_nc_eui2=AeEHnPGmedXnAfjysAqux7y0Rnn_sk8vKXlGef-yTy8peTj3s3yUy3hPvuHDMTu0MreiuXPFmsrnhko866MY92s4&_nc_ohc=W1S4aGl6by4AX90ZLEB&_nc_ht=scontent.fbkk5-7.fna&oh=03_AVLnb6pSuPPhbnlQW6XihTDVaYOur0CJBm_S5hhNvl6JLA&oe=6238AED0'),
            ),
            Container(
              //first
              color: Colors.orange,
              height: 32.0,
              width: 300.0,
              child: Text(
                //test
                "  | Quick Balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                ),
              ),
            ),
            Container(
              color: Colors.orange,
              height: 40.0,
              width: 300.0,
              child: Text(
                "      Add account or card to instantily check balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              height: 40.0,
              width: 300.0,
            ),
            Container(
              color: Colors.orange,
              height: 32.0,
              width: 300.0,
              child: Text(
                //second
                "  | My favorites",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                ),
              ),
            ),
            Container(
              color: Colors.orange,
              height: 40.0,
              width: 300.0,
              child: Text(
                "      Add account or card to instantily check balance",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              height: 40.0,
              width: 300.0,
            ),
          ],
        ),
      ),
    );
  }
}
